/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

import java.util.*;
import javax.swing.JOptionPane;

/**
 *
 * @author aleida
 */
public class Temperatura {

    private static int[][] estaciones = new int[4][3];

    public static int promedio() {
        int sumc = 0;
        for (int i = 0; i < estaciones.length; i++) {

            for (int j = 0; j < estaciones[0].length; j++) {
                sumc = sumc + estaciones[i][j];
            }
        }
        return sumc / 12;
    }

    public static int promedio2_4() {
        int sumc = 0;
        for (int i = 0; i < estaciones.length; i++) {
            i++;
            for (int j = 0; j < estaciones[0].length; j++) {
                sumc = sumc + estaciones[i][j];
            }
        }
        return sumc / 6;
    }

    public static String vector() {
        int x = promedio();
        String ex = "";
        for (int i = 0; i < estaciones.length; i++) {
            for (int j = 0; j < estaciones[0].length; j++) {
                if (x < estaciones[i][j]) {
                    ex += "temperatura: " + estaciones[i][j] + "\n";

                }
            }
        }

        return ex;
    }

    public static int mayor_promedio() {
        return 0;

    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("digite las temperaturas de la primera estación");
        for (int i = 0; i < 3; i++) {
            int aux = sc.nextInt();
            while (aux < 8 || aux > 32) {
               JOptionPane.showMessageDialog(null,"ingrese una temperatura desde 8 hasta 32");
                aux = sc.nextInt();
            }
            estaciones[0][i] = aux;
        }

        System.out.println("digite las temperaturas de la segunda estación");
        for (int i = 0; i < 3; i++) {
            int aux2=sc.nextInt();
            while(aux2 <8|| aux2> 32)
            { 
               JOptionPane.showMessageDialog(null,"ingrese una temperatura desde 8 hasta 32");
               aux2=sc.nextInt();
            }
            estaciones[1][i] = aux2;
        }

        System.out.println("digite las temperaturas de la tercera estación");
        for (int i = 0; i < 3; i++) {
            int aux3=sc.nextInt();
            while(aux3 <8|| aux3> 32)
            { 
               JOptionPane.showMessageDialog(null,"ingrese una temperatura desde 8 hasta 32");
               aux3=sc.nextInt();
            }
            estaciones[2][i] = aux3;
        }

        System.out.println("digite las temperaturas de la cuarta estación");
        for (int i = 0; i < 3; i++) {
         int aux4=sc.nextInt();
            while(aux4 <8|| aux4> 32)
            { 
               JOptionPane.showMessageDialog(null,"ingrese una temperatura desde 8 hasta 32");
               aux4=sc.nextInt();
            }
            estaciones[3][i] = aux4;
        }

        String salida = "";
        for (int i = 0; i < estaciones.length; i++) {

            for (int j = 0; j < estaciones[0].length; j++) {
                salida += "fila: " + i + " columna: " + j + " valor: " + estaciones[i][j] + "\n";
            }
        }
        System.out.println("es:" + salida);
        System.out.println("el promedio es: " + promedio());
        System.out.println("promedio de meses 2 y 4: " + promedio2_4());
        System.out.println("las temperaturas que estan mayor al promedio son: \n" + vector());
    }
}
